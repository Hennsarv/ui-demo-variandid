﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // konsooli rakendus - tüüpiliselt käivitatakse käsurealt
            // UI koosneb kahest funktsioonist-meetodist (ag amitte ainult)
            // Console.Write Console.WriteLine ja Console.ReadLine Console.ReadKey
            // Console.Beep(440, 1000);

            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine($"{i+1}. {args[i]}");
            }

            Console.Write("Kes sa oled: ");
            Console.WriteLine($"Tere {Console.ReadLine()}!");

            var aken = new Aken();
            aken.ShowDialog();


        }
    }
}
