﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace AndmetegaKonsoolKorda
{
    static class Program
    {
        static List<Inimene> raffas = null;
        static List<Inimene> Raffas => raffas ?? (raffas = GetPeople());

        static void Main(string[] args)
        {
            
            Console.WriteLine(string.Join("\n", Raffas));

            // kunagi hiljem tahaks veel

            

        }

        static List<Inimene> GetPeople()
        {
            /*
             * 1. ÜHENDADA END ANDMEBAASIGA
             * 2. Saata sinna küsimus
             * 3. Lugeda läbi vastus
             */

            
            string connectionString = "Data Source=valiitsql.database.windows.net;Initial Catalog=Northwind;User ID=student;Password=Pa$$w0rd";
            string commandText = "select * from employees";
            
            // 1.
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                 
                conn.Open();

                // 2.
                using (SqlCommand comm = new SqlCommand(commandText, conn))
                {
                    ;
                    using (SqlDataReader R = comm.ExecuteReader())
                    {

                        // 3.
                        while (R.Read()) //Console.WriteLine(R["FirstName"] +  " " + R["LastName"]);
                            new Inimene { Eesnimi = R["FirstName"].ToString(), Perenimi = R["LastName"].ToString() };
                    }
                }
                
            }

            return Inimene.Rahvas;
        }

    }

    class Inimene
    {
        public static List<Inimene> Rahvas = new List<Inimene>();
        static int mitu = 0;
        static int RahvaArv => mitu;

        public int Nr { get; } = ++mitu;
        public string Eesnimi { get; set; }
        public string Perenimi { get; set; }

        public Inimene() => Rahvas.Add(this);

        public override string ToString() => $"({Nr}) {Eesnimi} {Perenimi}";

    }
}
