﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEvent
{
    class Program
    {
        static void JuubliTervitus(Inimene x)
        {
            Console.WriteLine($"Võta torti {x.Nimi}!");
        }

        static void Main(string[] args)
        {
            var henn = new Inimene { Nimi = "Henn", Vanus = 65 };
            var ants = new Inimene { Nimi = "Ants", Vanus = 44 };

            //Console.WriteLine(henn);

            henn.Juubel += x => Console.WriteLine($"Palju õnne {x.Nimi} sa said {x.Vanus} aastaseks");
            ants.Juubel += JuubliTervitus;
            henn.Juubel += JuubliTervitus;

            for (int i = 0; i < 100; i++)
            {
                henn.Vanus++;
                Console.WriteLine(henn);
                ants.Vanus++;
                Console.WriteLine(ants);
            }
        }
    }

    class Inimene
    {
        public string Nimi { get; set; }
        int vanus;
        public int Vanus
        {
            get => vanus;
            set => ((vanus = value) % 25 == 0 ? Juubel : null)?.Invoke(this);
            //{ // ära tee nii nagu Henn, kirjuta pikalt välja
            //    vanus = value;
            //    if (vanus % 25 == 0) Juubel?.Invoke(this);
            //}
        }
        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";

        public Action<Inimene> Juubel;
    }

}
