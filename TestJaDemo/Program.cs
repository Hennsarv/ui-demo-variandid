﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestJaDemo
{
    static class Program
    {
        static void Main(string[] args)
        {
            Tryki(4);
            Tryki(DateTime.Today);

            string[] nimed = { "Henn", "Ants", "Peeter" };
            Console.WriteLine(nimed.ListStringiks());

            var nn = Massiiv(true, 100);


            MinuList<int> arvud = new MinuList<int>();
            arvud.Lisa(3);
            arvud.Lisa(4);

            for (int i = 0; i < arvud.Length; i++)
            {
                Console.WriteLine(arvud[i]);
            }

            arvud[0] = 77;

        }

        //static void Tryki(int x) => Console.WriteLine("integer {0}", x);

        //static void Tryki(double d) => Console.WriteLine("double {0}", d);

        static void Tryki<T>(T t) => Console.WriteLine("{0}: {1}", typeof(T), t);

        static string ListStringiks<T>(this IEnumerable<T> m) => string.Join(",", m);

        static T[] Massiiv<T>(T x, int s)
        {
            T[] uus = new T[s];
            for (int i = 0; i < s; i++)
            {
                uus[i] = x;
            }
            return uus;
        }

    }

    class MinuList<T> where T: IComparable
    {
        List<T> sisu = new List<T>();
        public void Lisa(T x) => sisu.Add(x);
        public IEnumerable<T> Sisu => sisu.AsEnumerable();

        public int Length => sisu.Count;

        public T this[int i]
        {
            get => sisu[i];
            set => sisu[i] = value;
        }

        public int? Leia(T t)
        {
            for (int i = 0; i < sisu.Count; i++)
            {
                if (sisu[i].CompareTo(t) == 0) return i;
            }
            return null;
        }
    }

}
