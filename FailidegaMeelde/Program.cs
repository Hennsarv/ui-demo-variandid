﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace FailidegaMeelde
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> Inimesed = new List<Inimene>
            {
                new Inimene {Nimi="Henn", Vanus=65},
                new Inimene {Nimi="Ants", Vanus=44},
                new Inimene {Nimi="Peeter", Vanus=22},
                new Inimene {Nimi="Jaak", Vanus=33},
            };

            string filename = @"..\..\inimesed.csv";
            string filenameJSON = @"..\..\inimesed.json";
            //File.WriteAllLines(filename, Inimesed.Select(x => $"{x.Nimi},{x.Vanus}"));

            //File.WriteAllText(filenameJSON, JsonConvert.SerializeObject(Inimesed));

            var loetud = JsonConvert.DeserializeObject<Inimene[]>(
                    File.ReadAllText(filenameJSON)
                );

            foreach (var i in loetud) Console.WriteLine($"{i.Nimi}-{i.Vanus}");

            var loetud2 = File.ReadAllLines(filename)
                .Select(x => x.Split(',')) //.Select(y => y.Trim()).ToArray())
                .Select(x => new Inimene { Nimi = x[0].Trim(), Vanus = int.Parse(x[1].Trim()) })
                .ToArray();
            foreach (var i in loetud2) Console.WriteLine($"{i.Nimi}-{i.Vanus}");
        }
    }

    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public int Kinganumber = 43;
    }

}
