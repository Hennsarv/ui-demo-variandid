﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndmetegaKonsoolLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            //foreach(var p in db.Products)
            //{
            //    Console.WriteLine(p.ProductName);
            //}

            //foreach (var c in db.Categories) Console.WriteLine(c.CategoryName);

            // see on LINQ keeles
            var q = from p in db.Products
                    where p.UnitPrice < 20
                    orderby p.UnitPrice descending
                    select new { p.ProductName, p.UnitPrice, p.Category.CategoryName };
            
            // see on Extensionite keeles
            var q1 = db.Products.Where(p => p.UnitPrice < 20)
                .OrderByDescending(p => p.UnitPrice)
                .Select(p => new { p.ProductName, p.UnitPrice, p.Category.CategoryName });

            foreach (var x in q) Console.WriteLine(x);

            
        }
    }
}
