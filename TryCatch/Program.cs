﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                try
                {
                    Console.Write("Anna üks arv: ");
                    int a = int.Parse(Console.ReadLine());

                    if (a == 7) throw new Exception("seitsmeid ma ei jaga");

                    Console.Write("Anna üks arv: ");
                    int b = int.Parse(Console.ReadLine());

                    Console.WriteLine($"Jagatis {a}/{b} on {a / b}");
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine("NUlliga ei saa jagada");
                }
                catch (OverflowException e)
                {
                    Console.WriteLine("Natsa suur arv");
                }
                catch (Exception e)
                {
                    Console.WriteLine("midagi läks untsu");
                    Console.WriteLine(e.Message);

                    throw e;
                }
                finally
                {
                    Console.WriteLine("rohkem ei viitsi");
                }

                Console.WriteLine("kas uuesti?");

            } while (Console.ReadKey().KeyChar != 'E');

        }
    }
}
