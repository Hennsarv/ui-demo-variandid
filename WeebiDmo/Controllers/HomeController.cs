﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeebiDmo.Models;

namespace WeebiDmo.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Tere(string id = "tundmatu")
        {
            ViewBag.Nimi = id;

            ViewBag.Nimed = new string[] { "Henn", "Ants", "Peter" };

            return View() ;
        }



        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}