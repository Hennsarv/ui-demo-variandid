﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;

namespace WeebiDmo.Models
{
    public class Inimene
    {

        public static List<Inimene> Rahvas = new List<Inimene>
        {
            new Inimene {Nimi = "Henn", Vanus= 65 },
            new Inimene {Nimi = "Ants", Vanus= 44 },
            new Inimene {Nimi = "Peeter", Vanus= 22 },
        };
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}