﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityConsoleDemo // kõige tavalisem Console rakendus
{
    partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }

    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            db.Database.Log = Console.WriteLine;

            #region kommentaarid
            // .    member call
            // ?.   conditional member call
            //          (avaldis)?.Member   // field või property
            //          (avaldis)?.Member() // meetod või funktsioon
            //          (avaldis) == null ? null : (avaldis).Member    - funktsioon või property
            //          if((avaldis) != null) (avaldis).Member();
            // ?? nulli asendamise avaldis
            //          (avaldis)??(asendus)
            //          (avaldis) == null ? (asendus) : (avaldis)
            // []   index call

            // kandiline null ja ümmargune null
            //          null    - mittemidagi       - kandiline null (HS termin)
            //          zero    - arv-null          - ümmargune null (HS termin)

            //Nullable<int> x = 7;
            //x = null;

            //Nullable<int>[] arvud = { 1, 3, 5 };
            //Console.WriteLine(arvud.Where(t => t%2==0).DefaultIfEmpty().Average()??7);

            #endregion

            //foreach (var p in               // funktsionaalne avaldis
            //    db.Products
            //    .Where(x => x.UnitPrice < 20)
            //    .OrderBy(x => x.UnitPrice)
            //    .Select(x => new { x.ProductName, x.UnitPrice })
            //    )
            //    //Console.WriteLine(p)
            //        ;

            var q =                // LINQ-avaldis (Language INtegrated Query)
                from x in db.Products
                where x.UnitPrice < 20
                orderby x.UnitPrice
                select new { x.ProductName, x.UnitPrice };

            //foreach (var p in q) Console.WriteLine(p);

            //var kala = db.Categories.Find(8);
            //Console.WriteLine(kala.CategoryName);

            ////kala.CategoryName = "Seafood";
            ////db.SaveChanges();

            //Category uus = new Category { CategoryName = "Muud asjad" };
            //db.Categories.Add(uus);

            //Console.Write("enne: ");
            //Console.WriteLine(uus.CategoryID);

            //db.SaveChanges();
            //Console.Write("peale: ");
            //Console.WriteLine(uus.CategoryID);

            //Category muud = db.Categories.Find(13);
            //db.Categories.Remove(muud);
            //db.SaveChanges();

            //Category kool = db.Categories.Where(x => x.CategoryName == "Kooliasjad").SingleOrDefault();
            //if(kool != null) { db.Categories.Remove(kool); db.SaveChanges(); }

            foreach(
                var p in db.Products
                .Include("Category")
                .Select(p => new { p.ProductName, p.UnitPrice, p.Category.CategoryName })
                .ToList()
                
                


                ) Console.WriteLine(p);


        }
    }
}
