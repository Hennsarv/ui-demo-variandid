﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EntityWindowsDemo
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Products.ToList();

            // algul teeme tühja list, mis sisaldab vaid valikut (sõna) All
            List<string> kategooriad = new List<string> { "All" };

            // siis lisame sinna baasist loetud kategooriate nimekirja (CategoryNamed)                
            kategooriad.AddRange(db.Categories.Select(x => x.CategoryName));
            
            // seejärel pistame selle listi comboboksi datasourceks
            this.comboBox1.DataSource = kategooriad;
            
            // ja nüüd toome komboboksi peidust välja
            this.comboBox1.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.SaveChanges();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Employees.ToList();
            this.comboBox1.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            this.comboBox1.Visible = false;
            db = null;
            db = new NorthwindEntities();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string valitud = comboBox1.SelectedItem.ToString();

            this.dataGridView1.DataSource
                = db.Products
                .Where(x => x.Category.CategoryName == valitud || valitud == "All")
                .ToList()
                ;
        }
    }
}
